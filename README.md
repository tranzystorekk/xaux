# X auxilliary utils

## About

A collection of utils I've deemed useful in my everyday Void Linux life.

### xrustdist

Downloads bootstrap distfiles for Rust and prints out their checksums,
useful for Rust package version bumps.

Requires `bash` and `curl`.
